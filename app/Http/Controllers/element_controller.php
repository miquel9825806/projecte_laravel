<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\element;
use voku\helper\ASCII;

class element_controller extends Controller
{
    public function index(){
        $element = element::all();
        return view('elements.index', compact('element'));
    }
    public function create()
    {
        return view('elements.create');
    }
    public function delete(element $element)
    {
        $element->delete();
        return redirect()->route('elements.index')->with('success', 'Element deleted successfully');
    }
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required|string',
            'dany_plus'=>'required|integer'
        ]);
        $element = new element([
           'name'=>$request->name,
            'dany_plus'=>$request->dany_plus
        ]);
        $element->save();
        return redirect()->route('elements.index')->with('success', 'Element added successfully');
    }
}
