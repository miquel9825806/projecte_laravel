<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class Category_Controller extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('categories.index', compact('categories'));
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'Percent_IVA' => 'required|numeric|min:0|max:100',
        ]);

        $category = new Category([
            'name' => $request->name,
            'Percent_IVA' => $request->Percent_IVA,
        ]);
        $category->save();

        return redirect()->route('categories.index')->with('success', 'Category added successfully');
    }

    public function delete(Category $category)
    {
        $category->delete();
        return redirect()->route('categories.index')->with('success', 'Category deleted successfully');
    }
}
