<?php
namespace App\Http\Controllers;
use App\Models\element;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;

class Product_Controller extends Controller
{
    public function create()
    {
        $categories = Category::all();
        $element = element::all();
        return view('products.create')->with(['categories' =>$categories, 'elements'=>$element]);
    }

public function store(Request $request)
{
    $request->validate([
        'name' => 'required|string|max:255',
        'quantity' => 'required|integer|min:1',
        'description' => 'required|string',
        'price' => 'required|numeric|min:1',
        'category_id' => 'required|exists:categories,id',
        'dany' => 'required|integer|min:1',
        'element_id' => 'required|exists:elements,id'
    ]);
    $element = element::find($request->element_id);
    $elementDamage = $element->dany_plus;
    $totalDamage = $request->dany + $elementDamage;

    $product = new Product([
        'name' => $request->name,
        'quantity' => $request->quantity,
        'description' => $request->description,
        'price' => $request->price,
        'category_id' => $request->category_id,
        'dany' => $request->dany,
        'element_id' => $request->element_id,
        'dany2' => $totalDamage
    ]);

    $product->save();

    return redirect()->route('products.index')->with('success', 'Product added successfully');
}

    public function index()
    {
        $products = Product::all();
        return view('products.index', compact('products'));
    }

    public function delete(Product $product) {
        $product->delete();
        return redirect()->route('products.index')->with('success', 'Product deleted successfully');
    }
}
