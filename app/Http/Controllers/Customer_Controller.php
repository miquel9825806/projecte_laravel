<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;

class Customer_Controller extends Controller
{
    public function index(){
        $customer = Customer::all();
        return view('customer.index', compact('customer'));
    }
    public function delete(Customer $customers){
        $customers->delete();
        return redirect()->route('customers.index')->with('success', 'Customer deleted successfully');;
    }
    public function store(Request $request) {
        $request->validate([
            'username'=>'required|string|max:255',
            'name' => 'required|string|max:255',
            'address' => 'required|string',
            'birthday' => 'required|date',
            'email' => 'required|string',
            'phone_number' => 'required|string|max:255'
        ]);
        $customer = new Customer([
            'username'=>$request->username,
            'name' => $request->name,
            'address' => $request->address,
            'birthday' => $request->birthday,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
        ]);

        $customer->save();
        return redirect()->route('customers.index')->with('success', 'Customer added successfully');
    }
}
