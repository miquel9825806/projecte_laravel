<?php
namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use App\Models\Customer;

class Order_controller extends Controller
{
    public function create()
    {
        $customers = Customer::all();
        $products = Product::where('quantity', '>', 0)->get();
        return view('orders.create', compact('customers', 'products'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'customer_id' => 'required|exists:customers,id',
            'product_id' => 'required|array',
            'product_id.*' => 'required|exists:products,id',
            'quantities' => 'required|array',
            'quantities.*' => 'required|integer|min:1'
        ]);

        $errors = [];
        foreach ($request->product_id as $key => $productId) {
            $quantity = $request->quantities[$key];
            $product = Product::find($productId);
            if ($quantity > $product->quantity) {
                $errors[] = "La quantidad sol·licitada per el producte {$product->name} excedeix la quantitat disponible.";
            }
        }

        if (!empty($errors)) {
            return redirect()->back()->withErrors($errors)->withInput();
        }

        foreach ($request->product_id as $key => $productId) {
            $quantity = $request->quantities[$key];
            $product = Product::find($productId);
            $totalPrice = $product->price * $quantity;
            $category = $product->category;

            if ($category && $category->Percent_IVA !== null) {
                $IVA = $category->Percent_IVA;
                $totalPriceWithIVA = $totalPrice + ($totalPrice * ($IVA / 100));
                if ($quantity > 0) {
                    Order::create([
                        'customer_id' => $request->customer_id,
                        'product_id' => $productId,
                        'quantity' => $quantity,
                        'total_price' => $totalPrice,
                        'IVA' => $IVA,
                        'total_price_with_IVA' => $totalPriceWithIVA,
                        'order_date' => now(),
                    ]);
                    $product->quantity -= $quantity;
                    $product->save();
                }
            } else {
                return redirect()->back()->withErrors(['La categoría del producte seleccionat no te un valor válid per el IVA.'])->withInput();
            }
        }

        return redirect()->route('orders.index')->with('success', 'Order Created Successfully');
    }


    public function index()
    {
        $orders = Order::with(['customer', 'product'])->get();
        return view('orders.index', compact('orders'));
    }

    public function delete(Order $order) {
        $order->delete();
        return redirect()->route('orders.index')->with('success', 'Order deleted Successfully');    }
}
