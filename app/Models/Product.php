<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'quantity', 'description', 'price', 'category_id', 'dany', 'element_id', 'dany2'];
    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }
    public function category() {
        return $this->belongsTo(Category::class);
    }
    public function element()
    {
        return $this->belongsTo(element::class);
    }
}
