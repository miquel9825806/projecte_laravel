<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class element extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'dany_plus'];
    public function product()
    {
        return $this->belongsToMany(Product::class);
    }
}
