<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    protected $fillable = ['username', 'name', 'address', 'birthday', 'email', 'phone_number'];
    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
