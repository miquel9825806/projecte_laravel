<?php


namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class create_customers_seed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('customers')->insert([
           ['username'=>'user1', 'name'=>'Lindsay Black', 'address'=>'23, Grand Street, London', 'birthday'=>'1983-03-15', 'email'=>'lindB@lostKingdom.com', 'phone_number'=>'+44 20 7123 4567', 'created_at'=>now(), 'updated_at'=>now()],
            ['username'=>'user2', 'name'=>'James White', 'address'=>'38, Small Street, Paris', 'birthday'=>'1999-05-31', 'email'=>'james@lostKingdom.com', 'phone_number'=>'+33 1 09 75 83 51', 'created_at'=>now(), 'updated_at'=>now()],
            ['username'=>'user3', 'name'=>'Lon Lin', 'address'=>'65, Gran Via, Barcelona', 'birthday'=>'2015-01-12', 'email'=>'lonlin@lostKingdom.com', 'phone_number'=>'+34 684 98 78 12', 'created_at'=>now(), 'updated_at'=>now()]
        ]);
    }
}
