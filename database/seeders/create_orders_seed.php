<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Order;
use App\Models\Product;

class create_orders_seed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach ($this->getOrderData() as $orderData) {
            $product = Product::find($orderData['product_id']);
            $category = $product->category;

            if ($category && $category->Percent_IVA !== null) {
                $totalPrice = $orderData['quantity'] * $product->price;
                $totalPriceWithIVA = $totalPrice + ($totalPrice * ($category->Percent_IVA / 100));
                $product->quantity -= $orderData['quantity'];
                $product->save();

                Order::create([
                    'customer_id' => $orderData['customer_id'],
                    'product_id' => $orderData['product_id'],
                    'quantity' => $orderData['quantity'],
                    'total_price' => $totalPrice,
                    'IVA' => $category->Percent_IVA,
                    'total_price_with_IVA' => $totalPriceWithIVA,
                    'order_date' => $orderData['order_date'],
                ]);
            }
        }
    }

    /**
     * Get order data.
     *
     * @return array
     */
    private function getOrderData(): array
    {
        return [
            [
                'customer_id' => 1,
                'product_id' => 1,
                'quantity' => 3,
                'order_date' => '2024-06-02',
            ],
            [
                'customer_id' => 2,
                'product_id' => 2,
                'quantity' => 2,
                'order_date' => '2024-06-03',
            ],
        ];
    }
}
