<?php


namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class create_element_seed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('elements')->insert([
            [
                'name'=>'none',
                'dany_plus'=>0,
                'created_at'=>now(),
                'updated_at'=>now()
            ],
            [
                'name'=>'fire',
                'dany_plus'=>15,
                'created_at'=>now(),
                'updated_at'=>now()
            ],
            [
                'name'=>'electric',
                'dany_plus'=>20,
                'created_at'=>now(),
                'updated_at'=>now()
            ],
            [
                'name'=>'dark',
                'dany_plus'=>50,
                'created_at'=>now(),
                'updated_at'=>now()
            ],
            ]);
    }
}
