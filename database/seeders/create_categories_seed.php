<?php


namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class create_categories_seed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('categories')->insert([
           [
               'name'=>'Swords',
               'Percent_IVA'=>15.00,
               'created_at'=>now(),
               'updated_at'=>now()
           ],
            [
                'name'=>'Armours',
                'Percent_IVA'=>20.00,
                'created_at'=>now(),
                'updated_at'=>now()
            ],
            [
                'name'=>'Spells',
                'Percent_IVA'=>18.50,
                'created_at'=>now(),
                'updated_at'=>now()
            ],
            [
                'name'=>'Shields',
                'Percent_IVA'=>10.00,
                'created_at'=>now(),
                'updated_at'=>now()
            ],
        ]);
    }
}
