<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Product;

class create_products_seed extends Seeder
{
    public function run(): void
    {
        $products = [
            [
                'name' => 'fire sword',
                'quantity' => 10,
                'description' => 'To burn and damage your enemies',
                'price' => 15.2,
                'category_id' => 1,
                'dany' => 13,
                'element_id' => 2
            ],
            [
                'name' => 'simple shield',
                'quantity' => 15,
                'description' => 'To defend yourself',
                'price' => 10,
                'category_id' => 4,
                'dany' => 10,
                'element_id' => 1
            ],
            [
                'name' => 'dark armour',
                'quantity' => 13,
                'description' => 'To go to the dark side',
                'price' => 13,
                'category_id' => 2,
                'dany' => 15,
                'element_id' => 4
            ]
        ];

        foreach ($products as $productData) {
            $product = Product::create($productData);
            $element = $product->element;
            $danyPlus = $element->dany_plus ?? 0;
            $product->dany2 = $product->dany + $danyPlus;
            $product->save();
        }
    }
}
