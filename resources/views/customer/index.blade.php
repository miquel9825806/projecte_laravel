<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Clients</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 20px;
            background-color: #f8f9fa;
        }
        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        h1 {
            text-align: center;
            margin-bottom: 20px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }
        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #f2f2f2;
        }
        button {
            display: block;
            margin: 20px auto;
            padding: 10px 20px;
            background-color: #007bff;
            color: #fff;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        button:hover {
            background-color: #0056b3;
        }
        .delete-link {
            color: red;
            cursor: pointer;
        }
        .delete-link:hover {
            text-decoration: underline;
        }
        .nav-menu {
            text-align: center;
            margin-top: 20px;
        }
        .nav-menu a {
            display: inline-block;
            margin: 0 10px;
            padding: 10px 20px;
            background-color: #007bff;
            color: #fff;
            text-decoration: none;
            border-radius: 4px;
        }
        .nav-menu a:hover {
            background-color: #0056b3;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="nav-menu">
        <a href="{{route('home')}}">Home</a>
        <a href="{{ route('categories.index') }}">Categories</a>
        <a href="{{ route('elements.index') }}">Elements</a>
        <a href="{{ route('products.index') }}">Products</a>
        <a href="{{ route('customers.index') }}">Customers</a>
        <a href="{{ route('orders.index') }}">Orders</a>
    </div>
    <h1>Llista de Clients</h1>
    <table>
        <thead>
        <tr>
            <th>ID Client</th>
            <th>Nom d'usuari</th>
            <th>Nom i cognoms</th>
            <th>Adreça</th>
            <th>Data naixement</th>
            <th>Email</th>
            <th>Telèfon</th>
            <th>Accions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($customer as $customers)
            <tr>
                <td>{{ $customers->id }}</td>
                <td>{{ $customers->username }}</td>
                <td>{{ $customers->name }}</td>
                <td>{{ $customers->address }}</td>
                <td>{{ $customers->birthday }}</td>
                <td>{{ $customers->email }}</td>
                <td>{{ $customers->phone_number }}</td>
                <td>
                    <a href="{{ route('customer.delete', ['customers' => $customers->id]) }}" class="delete-link" onclick="return confirm('Estàs segur que vols esborrar aquest client?')">Esborrar</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <button onclick="window.location='{{ route('customer.create') }}'">Afegeix un client</button>
</div>
</body>
</html>
