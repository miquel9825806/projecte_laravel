<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Afegir Clients</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 20px;
            background-color: #f8f9fa;
        }
        .container {
            max-width: 400px;
            margin: 20px auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        h1 {
            text-align: center;
            margin-bottom: 20px;
        }
        form {
            display: flex;
            flex-direction: column;
        }
        label {
            margin-bottom: 5px;
        }
        input[type="text"],
        input[type="date"] {
            width: 100%;
            padding: 8px;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
        button[type="submit"],
        button {
            padding: 10px 20px;
            background-color: #007bff;
            color: #fff;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        button[type="submit"]:hover,
        button:hover {
            background-color: #0056b3;
        }
        .back-button {
            display: block;
            margin-top: 20px;
            padding: 10px 20px;
            background-color: #ccc;
            color: #333;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            text-decoration: none;
            text-align: center;
        }
        .back-button:hover {
            background-color: #999;
        }
        .alert {
            color: #721c24;
            background-color: #f8d7da;
            border-color: #f5c6cb;
            padding: 10px;
            border-radius: 4px;
            margin-top: 20px;
            text-align: center;
        }
        .alert ul {
            list-style-type: none;
            padding: 0;
            margin: 0;
        }
        .alert li {
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>Afegir Client</h1>
    <form method="POST" action="{{ route('customers.store') }}">
        @csrf
        <label for="username">Nom d'usuari:</label>
        <input type="text" id="username" name="username" value="{{ old('username') }}" required>

        <label for="name">Nom:</label>
        <input type="text" id="name" name="name" value="{{ old('name') }}" required>

        <label for="address">Adreça:</label>
        <input type="text" id="address" name="address" value="{{ old('address') }}" required>

        <label for="birthday">Data Naixement:</label>
        <input type="date" id="birthday" name="birthday" value="{{ old('birthday') }}" required>

        <label for="email">Email:</label>
        <input type="text" id="email" name="email" value="{{ old('email') }}" required>

        <label for="phone_number">Num Telefon:</label>
        <input type="text" id="phone_number" name="phone_number" value="{{ old('phone_number') }}" required>

        <button type="submit">Afegir Client</button>
    </form>
    <a href="{{ route('customers.index') }}" class="back-button">Torna a la llista de clients</a>
</div>
@if ($errors->any())
    <div class="alert">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
</body>
</html>
