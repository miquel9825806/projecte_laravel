<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Elements</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 20px;
            background-color: #f8f9fa;
        }
        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        h1 {
            text-align: center;
            margin-bottom: 20px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }
        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #f2f2f2;
        }
        .delete-link {
            color: red;
            cursor: pointer;
        }
        .delete-link:hover {
            text-decoration: underline;
        }
        .add-button,
        .move-button {
            display: block;
            margin: 10px auto;
            padding: 10px 20px;
            background-color: #007bff;
            color: #fff;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            text-decoration: none;
            text-align: center;
            width: 150px;
        }
        .add-button:hover,
        .move-button:hover {
            background-color: #0056b3;
        }
        .button-container {
            text-align: center;
            margin-top: 20px;
        }
        .nav-menu {
            text-align: center;
            margin-top: 20px;
        }
        .nav-menu a {
            display: inline-block;
            margin: 0 10px;
            padding: 10px 20px;
            background-color: #007bff;
            color: #fff;
            text-decoration: none;
            border-radius: 4px;
        }
        .nav-menu a:hover {
            background-color: #0056b3;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="nav-menu">
        <a href="{{route('home')}}">Home</a>
        <a href="{{ route('categories.index') }}">Categories</a>
        <a href="{{ route('elements.index') }}">Elements</a>
        <a href="{{ route('products.index') }}">Products</a>
        <a href="{{ route('customers.index') }}">Customers</a>
        <a href="{{ route('orders.index') }}">Orders</a>
    </div>
    <h1>Llista de Elements</h1>
    <table>
        <thead>
        <tr>
            <th>ID Element</th>
            <th>Nom d'element</th>
            <th>Dany+/Defensa+</th>
            <th>Accions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($element as $elements)
            <tr>
                <td>{{ $elements->id }}</td>
                <td>{{ $elements->name }}</td>
                <td>{{ $elements->dany_plus }}</td>
                <td>
                    <a href="{{ route('elements.delete', ['element' => $elements->id]) }}" class="delete-link" onclick="return confirm('Estàs segur que vols esborrar aquest element?')">Esborrar</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <button class="add-button" onclick="window.location='{{ route('elements.create') }}'">Afegeix un element</button>
</div>
</body>
</html>
