<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 20px;
            background-color: #f8f9fa;
        }
        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        .nav-menu {
            text-align: center;
            margin-top: 20px;
        }
        .nav-menu a {
            display: inline-block;
            margin: 0 10px;
            padding: 10px 20px;
            background-color: #007bff;
            color: #fff;
            text-decoration: none;
            border-radius: 4px;
            transition: background-color 0.3s ease;
        }
        .nav-menu a:hover {
            background-color: #0056b3;
        }
        h1 {
            text-align: center;
            margin-bottom: 20px;
        }
        p {
            text-align: center;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="nav-menu">
        <a href="{{route('home')}}">Home</a>
        <a href="{{ route('categories.index') }}">Categories</a>
        <a href="{{ route('elements.index') }}">Elements</a>
        <a href="{{ route('products.index') }}">Products</a>
        <a href="{{ route('customers.index') }}">Customers</a>
        <a href="{{ route('orders.index') }}">Orders</a>
    </div>
    <h1>Pàgina d'inici</h1>
    <p>Benvinguts a aquesta pàgina de laravel, que simula una botiga d'armament en un joc de Rol, on pots personalitzar la tenda amb els productes, els clients i altres coses funcionals que serveixen en aquesta pàgina.</p>
    <img src="https://media.licdn.com/dms/image/D4E12AQEhSWRTreXYew/article-cover_image-shrink_600_2000/0/1694479065771?e=2147483647&v=beta&t=2fX6sWIIQzrJhYJfi6lFLXv42TExMElMe1QZaOexx4M">
</div>
    </body>
</html>
