<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Afegir Comanda</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f8f9fa;
        }
        .container {
            max-width: 800px;
            margin: 20px auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        h1 {
            text-align: center;
            margin-bottom: 20px;
        }
        form {
            display: flex;
            flex-direction: column;
            align-items: center;
        }
        label {
            margin-bottom: 8px;
        }
        select, input[type="number"] {
            width: 100%;
            padding: 8px;
            margin-bottom: 20px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
        }
        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #f2f2f2;
        }
        button[type="submit"] {
            padding: 10px 350px;
            background-color: #007bff;
            color: #fff;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        button[type="submit"]:hover {
            background-color: #0056b3;
        }
        .back-button {
            display: block;
            margin-top: 20px;
            padding: 10px 20px;
            background-color: #ccc;
            color: #333;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            text-decoration: none;
            text-align: center;
        }
        .back-button:hover {
            background-color: #999;
        }
        .alert {
            color: #721c24;
            background-color: #f8d7da;
            border-color: #f5c6cb;
            padding: 10px;
            border-radius: 4px;
            margin-top: 20px;
            text-align: center;
        }
        .alert ul {
            list-style-type: none;
            padding: 0;
            margin: 0;
        }
        .alert li {
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>Afegir Comanda</h1>
    <form method="POST" action="{{ route('orders.store') }}">
        @csrf
        <label for="customer">Client:</label>
        <select id="customer" name="customer_id" required>
            @foreach($customers as $customer)
                <option value="{{ $customer->id }}">{{ $customer->name }}</option>
            @endforeach
        </select>

        <label>Producte:</label>
        <table>
            <thead>
            <tr>
                <th>ID</th>
                <th>Nom</th>
                <th>Quantitat Disponible</th>
                <th>Preu Unitari</th>
                <th>Quantitat a comprar</th>
                <th>Preu Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->quantity }}</td>
                    <td>{{ $product->price }} €</td>
                    <td>
                        <input type="number" name="quantities[]" value="0" min="0" data-product-id="{{ $product->id }}" required>
                        <input type="hidden" name="product_id[]" value="{{ $product->id }}">
                    </td>
                    <td id="total_price_{{ $product->id }}">0.00 €</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <button type="submit">Afegir Comanda</button>
    </form>

    <a href="{{ route('orders.index') }}" class="back-button">Torna a la llista de comandes</a>
</div>
@if ($errors->any())
    <div class="alert">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
</body>
</html>
