<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Comandes</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 20px;
            background-color: #f8f9fa;
        }
        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        h1 {
            text-align: center;
            margin-bottom: 20px;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }
        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }
        th {
            background-color: #f2f2f2;
        }
        .delete-link {
            color: red;
            cursor: pointer;
        }
        .delete-link:hover {
            text-decoration: underline;
        }
        .add-button {
            display: block;
            margin: 20px auto;
            padding: 10px 20px;
            background-color: #007bff;
            color: #fff;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            text-decoration: none;
            text-align: center;
            width: 150px;
        }
        .add-button:hover {
            background-color: #0056b3;
        }
        .nav-menu {
            text-align: center;
            margin-top: 20px;
        }
        .nav-menu a {
            display: inline-block;
            margin: 0 10px;
            padding: 10px 20px;
            background-color: #007bff;
            color: #fff;
            text-decoration: none;
            border-radius: 4px;
        }
        .nav-menu a:hover {
            background-color: #0056b3;
        }
        .date-column {
            text-align: center;
            white-space: nowrap;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="nav-menu">
        <a href="{{route('home')}}">Home</a>
        <a href="{{ route('categories.index') }}">Categories</a>
        <a href="{{ route('elements.index') }}">Elements</a>
        <a href="{{ route('products.index') }}">Products</a>
        <a href="{{ route('customers.index') }}">Customers</a>
        <a href="{{ route('orders.index') }}">Orders</a>
    </div>
    <h1>Comandes</h1>
    <table>
        <thead>
        <tr>
            <th>ID Comanda</th>
            <th>ID Client</th>
            <th>ID Producte</th>
            <th>Quantitat</th>
            <th>Preu Total Sense IVA</th>
            <th>IVA</th>
            <th>Preu Total Amb IVA</th>
            <th class="date-column">Data Ordre</th>
            <th>Accions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($orders as $order)
            <tr>
                <td>{{ $order->id }}</td>
                <td>{{ $order->customer_id }}</td>
                <td>{{ $order->product_id }}</td>
                <td>{{ $order->quantity }}</td>
                <td>{{ $order->total_price }}€</td>
                <td>{{ $order->IVA }}%</td>
                <td>{{ $order->total_price_with_IVA }}€</td>
                <td class="date-column">{{ $order->order_date }}</td>
                <td>
                    <a href="{{ route('orders.delete', ['order' => $order->id]) }}" class="delete-link" onclick="return confirm('Estàs segur que vols esborrar aquesta comanda?')">Esborrar</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <button class="add-button" onclick="window.location='{{ route('orders.create') }}'">Afegeix una comanda</button>
</div>
</body>
</html>
