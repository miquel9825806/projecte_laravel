<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Afegir Producte</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 20px;
            background-color: #f8f9fa;
        }
        .container {
            max-width: 600px;
            margin: 0 auto;
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        h1 {
            text-align: center;
            margin-bottom: 20px;
        }
        form {
            display: flex;
            flex-direction: column;
        }
        label {
            margin-bottom: 8px;
        }
        input[type="text"],
        input[type="number"],
        textarea,
        select {
            width: 100%;
            padding: 10px;
            margin-bottom: 20px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }
        button[type="submit"],
        button {
            padding: 10px 20px;
            background-color: #007bff;
            color: #fff;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        button[type="submit"]:hover,
        button:hover {
            background-color: #0056b3;
        }
        .back-button {
            display: block;
            margin-top: 20px;
            padding: 10px 250px;
            background-color: #ccc;
            color: #333;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            text-decoration: none;
            text-align: center;
        }
        .back-button:hover {
            background-color: #999;
        }
        .alert {
            color: #721c24;
            background-color: #f8d7da;
            border-color: #f5c6cb;
            padding: 10px;
            border-radius: 4px;
            margin-top: 20px;
            text-align: center;
        }
        .alert ul {
            list-style-type: none;
            padding: 0;
            margin: 0;
        }
        .alert li {
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
<div class="container">
    <h1>Afegir Producte</h1>
    <form method="POST" action="{{ route('product.store') }}">
        @csrf
        <label for="name">Nom:</label>
        <input type="text" id="name" name="name" value="{{ old('name') }}" required>

        <label for="quantity">Quantitat:</label>
        <input type="number" id="quantity" name="quantity" value="{{ old('quantity') }}" required>

        <label for="description">Descripció:</label>
        <textarea id="description" name="description">{{ old('description') }}</textarea>

        <label for="price">Preu:</label>
        <input type="number" step="0.01" id="price" name="price" value="{{ old('price') }}" required>

        <label for="category_id">Categoria:</label>
        <select id="category_id" name="category_id" required>
            @foreach($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
        </select>

        <label for="dany">Dany Base:</label>
        <input type="number" step="0.01" id="dany" name="dany" value="{{ old('dany') }}" required>

        <label for="element_id">Element:</label>
        <select id="element_id" name="element_id" required>
            @foreach($elements as $element)
                <option value="{{$element->id}}">{{$element->name}}</option>
            @endforeach
        </select>

        <button type="submit">Afegir Producte</button>
    </form>
    <button onclick="window.location='{{ route('products.index') }}'" class="back-button">Torna a la llista de productes</button>
</div>
@if ($errors->any())
    <div class="alert">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
</body>
</html>
