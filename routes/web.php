<?php

use App\Http\Controllers\Category_Controller;
use App\Http\Controllers\Customer_Controller;
use App\Http\Controllers\Order_controller;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Product_Controller;
use App\Http\Controllers\element_controller;
use \App\Http\Controllers\dany_element_controller;
Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auths', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});
Route::get('/products', [Product_Controller::class, 'index'])->name('products.index');
Route::get('/product/create', [Product_Controller::class, 'create'])->name('products.create');
Route::post('/product', [Product_Controller::class, 'store'])->name('product.store');
Route::get('/product/{product}/delete', [Product_Controller::class, 'delete'])->name('product.delete');
Route::get('/customers', [Customer_Controller::class, 'index'])->name('customers.index');
Route::get('/customer/{customers}/delete', [Customer_Controller::class, 'delete'])->name('customer.delete');
Route::get('/customer/create', function () {
    return view('customer.create');
})->name('customer.create');
Route::post('/customer', [Customer_Controller::class, 'store'])->name('customers.store');
require __DIR__.'/auth.php';
Route::get('/orders', [Order_controller::class, 'index'])->name('orders.index');
Route::get('/order/{order}/delete', [Order_controller::class, 'delete'])->name('orders.delete');
Route::get('/order/create', function () {
    return view('orders.create');
})->name('orders.create');
Route::post('/order', [Order_controller::class, 'store'])->name('orders.store');
Route::resource('orders', Order_controller::class);
Route::get('/categories', [Category_Controller::class, 'index'])->name('categories.index');
Route::get('/category/{category}/delete', [Category_Controller::class, 'delete'])->name('categories.delete');
Route::get('/category/create', function () {
    return view('categories.create');
})->name('categories.create');
Route::post('/category', [Category_Controller::class, 'store'])->name('categories.store');
Route::resource('categories', Category_Controller::class)->except(['show', 'edit', 'update']);
Route::get('/elements', [element_controller::class, 'index'])->name('elements.index');
Route::get('/element/{element}/delete', [element_controller::class, 'delete'])->name('elements.delete');
Route::get('/element/create', function () {
    return view('elements.create');
})->name('elements.create');
Route::post('/element', [element_controller::class, 'store'])->name('elements.store');
Route::resource('elements', element_controller::class)->except(['show', 'edit', 'update']);
